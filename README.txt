This repository contains a build directory for standalone native client
checkouts. In a standalone Native Client checkout, the root directory is not
under source control (everything is DEPSed in underneath it) so all toplevel
directories must be separate repositories. This is needed for some GN
configuration so that it is in an absolute location relative to the source
root.
